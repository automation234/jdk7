FROM registry.kube.local:8083/java/jdk7:u80

ARG PROFILE
RUN    yum install -y unzip wget \
     ; wget https://sourceforge.net/projects/jboss/files/JBoss/JBoss-4.0.5.GA/jboss-4.0.5.GA.zip/download -O /tmp/jboss-4.0.5.GA.zip  \
     ; unzip -d /opt/ /tmp/jboss-4.0.5.GA.zip \
     ; useradd jboss   


RUN    chown -R jboss.jboss /opt/jboss-4.0.5.GA \ 
     ; rm -rf /tmp/jboss-4.0.5.GA.zip

USER jboss
ENTRYPOINT ["/opt/jboss-4.0.5.GA/bin/run.sh", "-b","0.0.0.0","-c", "default"]
